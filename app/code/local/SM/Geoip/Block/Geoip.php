<?php

class SM_Geoip_Block_Geoip extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        $this->addData(array(
            'cache_lifetime' => 3600,
            'cache_tags' => array(Mage_Core_Model_Store::CACHE_TAG, Mage_Cms_Model_Block::CACHE_TAG, SM_Geoip_Model_Geoip::CACHE_TAG),
        ));
    }

    public function getCacheKeyInfo()
    {
        return array(
            'GEOIP_BLOCK',
            Mage::app()->getStore()->getId(),
            (int)Mage::app()->getStore()->isCurrentlySecure(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template')
        );
    }

    /**
     * Produce links list rendered as html
     *
     * @return string
     */
    public function getGeoip()
    {
        $html = '';
        $config_Freegeoip = Mage::helper('sm_geoip')->getGeoipConfig();
        if ($config_Freegeoip) {
            $infoType_config = Mage::helper('sm_geoip')->getGeoipConfigInfo();
            $infoType_config = explode(',', $infoType_config);
            $geoip = $this->getGeoipInfo();
            if (!is_null($geoip)) {
                foreach ($infoType_config as $option) {
                    $html .= $geoip->$option . ' | ';
                }
            }
        }
        return $html;
    }

    /**
     * get Geoip info from customer session if it exist
     *
     */
    private function getGeoipInfo()
    {
        $geoip = Mage::getSingleton('customer/session')->getData('geoip');
        if (is_null($geoip)) {
            return null;
        } else {
            return $geoip;
        }
    }
}