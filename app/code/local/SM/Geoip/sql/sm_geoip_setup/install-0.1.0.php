<?php

$installer = $this;
$installer->startSetup();

//==========================================================================
// Create GeoIp block
//==========================================================================
$blockTitle = "SM GeoIp";
$blockIdentifier = "sm_geoip";
$blockIsActive = 1;
$blockContent = <<<EOD
<p>Your country: <span>{{widget type="sm_geoip/geoip" sm_geoip_type="name"}}</span></p>
EOD;
$block = Mage::getModel('cms/block')->load($blockIdentifier);
if (!$block->getId()) {
    $staticBlock = array(
        'title' => $blockTitle,
        'identifier' => $blockIdentifier,
        'content' => $blockContent,
        'is_active' => 1,
        'stores' => array(0)
    );
    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

$installer->endSetup();
