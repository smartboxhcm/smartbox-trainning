<?php

class SM_Geoip_Model_System_Config_Backend_Info extends Mage_Adminhtml_Model_System_Config_Backend_Cache
{
    protected $_cacheTags = array(Mage_Core_Model_Store::CACHE_TAG, Mage_Cms_Model_Block::CACHE_TAG, SM_Geoip_Model_Geoip::CACHE_TAG);

    protected function _afterSave()
    {
        if ($this->isValueChanged()) {
            Mage::app()->cleanCache($this->_cacheTags);
        }
    }
}