<?php

class SM_Geoip_Model_System_Config_Source_Info
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'ip', 'label' => Mage::helper('adminhtml')->__('IP')),
            array('value' => 'country_code', 'label' => Mage::helper('adminhtml')->__('Country Code')),
            array('value' => 'country_name', 'label' => Mage::helper('adminhtml')->__('Country Name')),
            array('value' => 'region_code', 'label' => Mage::helper('adminhtml')->__('Region Code')),
            array('value' => 'region_name', 'label' => Mage::helper('adminhtml')->__('Region Name')),
            array('value' => 'city', 'label' => Mage::helper('adminhtml')->__('City')),
            array('value' => 'zip_code', 'label' => Mage::helper('adminhtml')->__('Zip Code')),
            array('value' => 'time_zone', 'label' => Mage::helper('adminhtml')->__('Time Zone')),
            array('value' => 'latitude', 'label' => Mage::helper('adminhtml')->__('Latitude')),
            array('value' => 'longitude', 'label' => Mage::helper('adminhtml')->__('Longitude')),
        );
    }
}