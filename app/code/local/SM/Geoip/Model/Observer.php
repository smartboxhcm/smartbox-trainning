<?php

class SM_Geoip_Model_Observer
{
    public function getGeoipInfo($observer)
    {
        $config_Freegeoip = Mage::helper('sm_geoip')->getGeoipConfig();
        if ($config_Freegeoip) {
            $geoip = Mage::getSingleton('customer/session')->getData('geoip');
            if (is_null($geoip)) {
                $url = 'http://freegeoip.net/json/';
                $curl = new Varien_Http_Adapter_Curl();
                $curl->setConfig(array(
                    'timeout' => 3
                ));
                $curl->write(Zend_Http_Client::GET, $url);
                $data = $curl->read();
                if ($data === false) {
                    return false;
                }

                $data = preg_split('/^\r?$/m', $data, 2);
                $data = trim($data[1]);
                $curl->close();

                try {
                    $data = json_decode($data);
                    Mage::getSingleton('customer/session')->setData('geoip', $data);
                    return $data;
                } catch (Exception $e) {
                    return null;
                }
            }
        }
    }
}