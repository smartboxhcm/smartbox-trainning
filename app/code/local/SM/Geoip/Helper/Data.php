<?php

class SM_Geoip_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getGeoipConfig(){
        $store_id = Mage::app()->getStore()->getStoreId();
        $configure = Mage::getStoreConfig('sm_geoip/freegeoip/active', $store_id);
        return $configure;
    }

    public function getGeoipConfigInfo(){
        $store_id = Mage::app()->getStore()->getStoreId();
        $configure = Mage::getStoreConfig('sm_geoip/freegeoip/info', $store_id);
        return $configure;
    }
}