<?php

/**
 * Class SM_Featuredproduct_Block_Featuredproduct
 *
 * @category   SM
 * @package    SM_Featuredproduct
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     HanNN <hannn@smartosc.com>
 * @copyright  2015 SmartBox/SmartOSC
 */
class SM_Featuredproduct_Block_Featuredproduct extends Mage_Catalog_Block_Product_Abstract
{
    const _NUMBER_SHOWN_PRODUCT = 10;

    protected function _construct()
    {
        parent::_construct();
        $this->getFeaturedProducts();
    }//end _construct()

    /**
     * getFeaturedProducts function
     * Get products collection for all products or in a category
     * Then adding the result to products collection to fetch out later
     * @return NULL
     */
    public function getFeaturedProducts()
    {
        $category = Mage::registry('current_category');
        if ($category) {
            $categoryId = $category->getId();
            $products = Mage::getSingleton('catalog/category')
                ->load($categoryId)
                ->getProductCollection();
        } else {
            $products = Mage::getResourceModel('catalog/product_collection');
        }

        $limitNumber = $this->getNumberShownProduct();

        $products->addAttributeToSelect('*')
            ->addAttributeToFilter('sm_featured_product', array('eq' => '1'))
            ->addStoreFilter();
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        // Mage::getSingleton('catalog/product_visibility')->addVisibleInSearchFilterToCollection($products);
        $products->setPageSize($limitNumber)
            ->setCurPage(1)
            ->load();
        $this->setProductCollection($products);
    }//end getFeaturedProducts()

    /**
     * getNumberShownProduct function
     * Return the limit number of products need to be shown on slider
     * @return int
     */
    public function getNumberShownProduct()
    {
        return self::_NUMBER_SHOWN_PRODUCT;
    }//end getNumberShownProduct()

}//end class
