<?php

/**
 * Class SM_PriceRelatedProducts_Block_Catalog_Product_List_Related
 */
class SM_PriceRelatedProducts_Block_Catalog_Product_List_Related extends Mage_Catalog_Block_Product_List_Related
{
    /**
     * @return $this
     */
    protected function _prepareData()
    {
        $product = Mage::registry('product');
        /* @var $product Mage_Catalog_Model_Product */

        $this->_itemCollection = $product->getRelatedProductCollection()
            ->addAttributeToSelect('required_options')
            ->setPositionOrder()
            ->addStoreFilter();
        $this->_addProductConditions();
        $this->_itemCollection->load();

        if ($this->_itemCollection->getSize() == 0 && Mage::helper('pricerelatedproducts')->isActive()) {
            $this->_itemCollection = $product->getRelatedProductsByPrice();
            $this->_addProductConditions();
            $this->_itemCollection->load();
        }

        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }

        return $this;
    }

    private function _addProductConditions()
    {
        if (Mage::helper('catalog')->isModuleEnabled('Mage_Checkout')) {
            Mage::getResourceSingleton('checkout/cart')->addExcludeProductFilter($this->_itemCollection,
                Mage::getSingleton('checkout/session')->getQuoteId()
            );
            $this->_addProductAttributesAndPrices($this->_itemCollection);
        }
//        Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($this->_itemCollection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($this->_itemCollection);
    }
}