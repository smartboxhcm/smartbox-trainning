<?php

/**
 * Class SM_PriceRelatedProducts_Helper_Data
 *
 * PHP version 5*
 *
 * @category  Products
 * @package   Catalog
 * @author    Tuan Nguyen <ng.nhat.tuan@gmail.com>
 * @copyright 2015 Tuan Nguyen
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://smartboxhcm.local/
 */
class SM_PriceRelatedProducts_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Get Price Related Products module configuration
     * @return mixed
     */
    public function getConfiguration()
    {
        return Mage::getStoreConfig('sm_pricerelatedproducts/pricerelatedproducts');
    }

    /**
     * Use to check allow run extension or not
     * @return bool
     */
    public function isActive()
    {
        return (bool)Mage::getStoreConfig('sm_pricerelatedproducts/pricerelatedproducts/active');
    }
}