<?php

/**
 * Class SM_PriceRelatedProducts_Model_Product
 *
 * Extend to show related pricing products
 *
 * PHP version 5*
 *
 * @category  Products
 * @package   Catalog
 * @author    Tuan Nguyen <ng.nhat.tuan@gmail.com>
 * @copyright 2015 Tuan Nguyen
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://smartboxhcm.local/
 */
class SM_PriceRelatedProducts_Model_Product extends Mage_Catalog_Model_Product
{
    /**
     * Get products have similar pr
     * @return Mage_Catalog_Model_Resource_Product_Collection
     * @throws Mage_Core_Exception
     */
    public function getRelatedProductsByPrice()
    {
        $productId = $this->getId();
        $configuration = Mage::helper('pricerelatedproducts')->getConfiguration();
        $limit = $configuration['limit'];
        $price = (float)$this->getFinalPrice();
        $collection = Mage::getModel('catalog/product')->getCollection();
        $math = "ABS({{attribute}} - $price)";
        $collection->addExpressionAttributeToSelect('sub_price', $math, 'price');
        $collection->addIdFilter($productId, true);
        $collection->addAttributeToFilter('price', array('notnull' => true));
        $collection->getSelect()
            ->order('sub_price')
            ->limit($limit);
        return $collection;
    }
}