<?php
/**
 * Bestseller List
 *
 * @category   SM
 * @package    SM_Bestseller
 */
class SM_Bestseller_Block_List extends Mage_Catalog_Block_Product_Abstract
{
    public function getBestsellerProducts($number = 10)
    {
        $collection = Mage::getModel('bestseller/list')->getBestsellerProducts($number);
        return $collection;
    }
}