<?php
/**
 * Bestseller List
 *
 * @category   SM
 * @package    SM_Bestseller
 */
class SM_Bestseller_Model_List extends Mage_Core_Model_Abstract
{
    public function getBestsellerProducts($number)
    {
        $storeId = (int) Mage::app()->getStore()->getId();
        $currentCategory = Mage::registry('current_category');

        // Date
        $date = new Zend_Date();
        $toDate = $date->setDay(1)->getDate()->get('Y-MM-dd');
        $fromDate = $date->subMonth(1)->getDate()->get('Y-MM-dd');

        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addStoreFilter()
            ->addPriceData()
            ->addTaxPercents()
            ->addUrlRewrite();

        if ($currentCategory) {
            $collection->addCategoryFilter($currentCategory);
        }

        $collection->getSelect()->join(
            array('aggregation' => $collection->getResource()->getTable('sales/bestsellers_aggregated_monthly')),
            "e.entity_id = aggregation.product_id AND aggregation.store_id={$storeId} AND aggregation.period BETWEEN '{$fromDate}' AND '{$toDate}'",
            array('SUM(aggregation.qty_ordered) AS sold_quantity')
        )
        ->group('e.entity_id')
        ->order(array('sold_quantity DESC', 'e.created_at'));

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        $collection->setPageSize($number);

        return $collection;
    }
}